$(document).ready(function() {
    var desktopImagesArray = [
        ["./assets/amex-logo.png", "./assets/avi-networks-logo.png", "./assets/blume-global-logo.png", "./assets/8X8-logo.png"],
        ["./assets/grab-on-logo.png", "./assets/nokia-logo.png", "./assets/redseal-logo.png", "./assets/culture-machine-logo.png", ],
        ["./assets/cdw-logo.png", "./assets/citrix-logo.png", "./assets/cleargage-logo.png", "./assets/wikibon-logo.png"],
        ["./assets/earlens-logo.png", "./assets/ottoq-logo.png", "./assets/storage-i-logo.png", "./assets/modria-logo.png"],
        ["./assets/silicon-angle-logo.png", "./assets/cloud-physics-logo.png", "./assets/tekion-logo.png", "./assets/cloudknox-logo.png"]
    ]; // array of images for screens greater than 767

    var tabImagesArray = [
        ["./assets/amex-logo.png", "./assets/avi-networks-logo.png", "./assets/blume-global-logo.png", "./assets/8X8-logo.png", "./assets/silicon-angle-logo.png", "./assets/cloud-physics-logo.png"],
        ["./assets/grab-on-logo.png", "./assets/nokia-logo.png", "./assets/redseal-logo.png", "./assets/culture-machine-logo.png", "./assets/tekion-logo.png", "./assets/cloudknox-logo.png"],
        ["./assets/cdw-logo.png", "./assets/citrix-logo.png", "./assets/cleargage-logo.png", "./assets/wikibon-logo.png", "./assets/earlens-logo.png", "./assets/ottoq-logo.png"]
    ]; // array of images for screens greater than 767

    var data = screen.width > 768 ? desktopImagesArray : tabImagesArray;

    // client img-section logic
    var outerIndex = 0; //number of inner arrays in imagesData
    var innerIndex = 1; // number of elements in the inner arrays

    //loading the images to #1, #2,#3,#4,#5 id's on Dom Load
    for (let index = 0; index < data.length; index++) {
        $('#' + parseInt(index + 1)).attr('src', data[index][0]);
    }

    setInterval(function() { // interval function that executes for every 3 seconds
        $('#' + parseInt(outerIndex + 1)).fadeOut(1000, function() { // fadeout function
            $('#' + parseInt(outerIndex + 1)).attr('src', data[outerIndex][innerIndex]).fadeIn(1000);
            outerIndex++;
            if (screen.width > 768) { //  condition for devices greater than 768px and less than 768px
                if (outerIndex % 5 == 0) { // making outerIndex 0 when array's count is reached 5
                    outerIndex = 0;
                    innerIndex++;
                }
                if (innerIndex % 4 == 0) { //making innerIndex 0 when elements count is reached 4
                    innerIndex = 0;
                }
            } else {
                if (outerIndex % 3 == 0) { //making outerIndex 0 when array count is reached 3 for tab and mobile devices
                    outerIndex = 0;
                    innerIndex++;
                }
                if (innerIndex % 6 == 0) { //making innerIndex 0 when elements count is reached 6 for tab and mobile devices
                    innerIndex = 0;
                }
            }
        })
    }, 3000);

    // Add minus icon for collapse element which is open by default
    // $(".collapse.show").each(function() {
    //     $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    // });

    // Toggle plus minus icon on show hide of collapse element
    $(".faq-question").on('click', function() {

        $(this).prev(".faq-card").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('click', function() {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });

    //faq accordian icons

    // var minusIcon = 'assets/minus-icon.png'
    // var plusIcon = 'assets/plus-icon.png'

    // $('.faq-question').on('click', function(e) {
    //     var imgSrc = $(this).find('img').attr('src');
    //     var icon = imgSrc == 'assets/plus-icon.png' ? minusIcon : plusIcon;
    //     $(this).find('img').attr('src', icon);
    // })


    // navigating to particular section when it is clicked
    //faq page side bar options

    $('#list-item-pricing').on('click', function() {
        $('.faq-pricing-img').attr('src', 'assets/faq-pricing.png');
    });
    $('#list-item-other').on('click', function() {
        $('.faq-pricing-img').attr('src', 'assets/faq-others.png');
    });
    $('#list-item-service').on('click', function() {
        $('.faq-pricing-img').attr('src', 'assets/faq-services.png');
    });

    //navbar togller
    $('.bar').on('click', function() {
        $('.bar').toggleClass('open');
    })

    //roataing chevron icon when faq question is clicked
    //var rightIcon = 'fa fa-chevron-right';
    // var downIcon = 'fa fa-chevron-down';
    $('.service-slide').on('click', function() {
        var accordianIcon = $(this).find('span');
        //var directionIcon = accordianIcon == 'fa fa-chevron-down' ? rightIcon : downIcon;
        accordianIcon.toggleClass('rotated-icon')
    })

    // making sidebar fixed when page is scrolling
    const sideBar = $('.faq-options');
    if ($('.faq-options').length) {
        var element = $('.faq-options'); //element access
        var stickyTop = $('.faq-options').offset().top - 100; // calculating the height between top and element
        var stickyHeight = element.height(); //
        $(window).scroll(function() {
            var limit = $('footer').offset() - stickyHeight - 20;
            var windowTop = $(window).scrollTop();
            if (stickyTop < windowTop) {
                element.css({
                    position: 'fixed',
                    top: '104px',
                    width: '350px',
                    //     padding: '0'
                });
                $('.offerings-sidebar').css('width', '359px !important')
                $('.faq-options-list').css({
                    'padding-left': '1%'
                })
            } else {
                $('.faq-options').css({
                    // 'padding-left': '3%'
                    top: '250px'
                })
            }

            if (limit < windowTop) {
                var diff = limit - windowTop;
                ele.css({
                    top: diff
                });
            }
        });
    }

    //offerings page
    // making sidebar fixed when page is scrolling
    const sideBarOffering = $('.offerings-sidebar');
    if ($('.offerings-sidebar').length) {
        var elementOffering = $('.offerings-sidebar'); //element access
        var stickyTop = $('.offerings-sidebar').offset().top - 100; // calculating the height between top and element
        var stickyHeight = elementOffering.height(); //
        $(window).scroll(function() {
            var limit = $('footer').offset() - stickyHeight - 20;
            var windowTop = $(window).scrollTop();
            if (stickyTop < windowTop) {
                elementOffering.css({
                    position: 'fixed',
                    top: '104px',
                    width: '350px',
                    //     padding: '0'
                });
                $('.offerings-sidebar').css('width', '359px')
                $('.faq-options-list').css('padding-left', '5.55%')
            } else {
                $('.offerings-sidebar').css({
                    top: '332px'
                })
            }

            if (limit < windowTop) {
                var diff = limit - windowTop;
                elementOffering.css({
                    top: diff
                });
            }
        });
    }


    //faq page scrolling logic

    // $(window).scroll(function() {
    //     if ($(window).scrollTop() < 350) {
    //         $('#service').css('padding-top', '0');
    //         $('#pricing').css('padding-top', '0');
    //     }
    // })


    $('#list-item-service').on('click', function() {
        $('#service').css('padding-top', '11%');
    });
    $('#list-item-pricing').on('click', function() {
        $('#pricing').css('padding-top', '11%');
        $('#service').css('padding-top', '0');
    });
    $('#list-item-other').on('click', function() {
        $('#other').css('padding-top', '11%');
    });

    $('.faq-other-button').on('click', function() {
        $(this).css('margin-bottom', '0')
    })

    // navigating to particular section when it is clicked
    //offerings  page side bar options

    $('#list-item-experience').on('click', function() {
        $('#experience').css('padding-top', '11%');
        $('#usability').css('padding-top', '0');
        $('#product').css('padding-top', '0%');
    });
    $('#list-item-usability').on('click', function() {
        $('#experience').css('padding-top', '0');
        $('#usability').css('padding-top', '11%');
        $('#product').css('padding-top', '0%');
    });
    $('#list-item-product').on('click', function() {
        $('#experience').css('padding-top', '0');
        $('#usability').css('padding-top', '0');
        $('#product').css('padding-top', '11%');
    });

    // adding collpase class to main accordian in faq and offerings pages
    if (screen.width < 768) {
        console.log('executing');
        $('.accordian-item').addClass('collapse');
    }

    // contact us page form methods

    $('#user-input-name, #user-input-data, #user-input-mail').on('keyup', function() {
            if ($('#user-input-name').val() != " " && $('#user-input-data').val() != "" && $('#user-input-mail').val() != "") {
                $('#submit-form').css({
                    'background-color': '#e0406e',
                    'pointer-events': 'all'
                })

            } else {
                $('#submit-form').css({
                    'background-color': '#e1e1e6',
                    'pointer-events': 'all'
                })

            }

        })
        //----------------------contact us form validation--------------------//
    $('#submit-form').on('click', function(e) {
            e.preventDefault();
            var userName = $('#user-input-name').val();
            var userData = $('#user-input-data').val();
            var userEmail = $('#user-input-mail').val();
            if (dataValidate(userName, userData, userEmail)) {
                $('.form-block').css('display', 'none')
                $('.response-container').css('display', 'block')
            } else {
                $('.email-feild').append('<p class="form-error">' + 'Sorry, this doesn’t look like a valid email' + '</p>')
                $('#user-input-mail').toggleClass('invalid-email')
            }
        })
        //----------------------response container button function--------------------//
    $('#response-button').click(function() {
        $('.form-block').css('display', 'block')
        $('.response-container').css('display', 'none')
        window.location.href = 'contact.html';
        document.querySelector('.contact-form').reset();

    })
    var emailRegex = /^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-zA-Z]{2,4}$/;
    //contact us form validation function----------------//
    function dataValidate(userName, userData, userEmail) {
        Console.log(userName, userEmail)
        if (userName.length == "") {
            return false;
        }
        if ((emailRegex.test(userEmail))) {
            return true;

        } else {
            $('.aboutus-email-feild').append('<p class="form-error">' + 'Sorry, this doesn’t look like a valid email' + '</p>')
            $('#aboutus-user-email').toggleClass('invalid-email')
            return false;
        }

        return true
    }


    //------------faq page form validation
    $('#faq-user-form-data, #faq-form-user-email').on('keyup', function() {

        if ($('#faq-user-form-data').val() != "" && $('#faq-form-user-email').val() != "") {
            $('#faq-submit-form-button').css({
                'background-color': '#e0406e',
                'pointer-events': 'all'
            })

        } else {
            $('#faq-submit-form-button').css({
                'background-color': '#e0406e',
                'pointer-events': 'all'
            })

        }

    })

    $('#faq-submit-form-button').on('click', function(e) {
        e.preventDefault();
        var faqUserData = $('#faq-user-form-data').val();
        var faqUserEmail = $('#faq-user-form-email').val();
        console.log(faqUserEmail);
        if (emailRegex.test(faqUserEmail)) {
            $('.faq-contact-form-block').css('display', 'none')
            $('.faq-response-container').css('display', 'block')
        } else {
            $('.mail-feild').append('<p class="form-error">' + 'Sorry, this doesn’t look like a valid email' + '</p>')
            $('#faq-user-form-email').toggleClass('invalid-email');
        }
    })
    $('#faq-response-button').on('click', function() {
        window.location.href = "faq.html"
    })

    //about us page form validation---------------------------------------------------------------------------------
    $('#aboutus-user-name, #aboutus-user-data, #aboutus-user-email').on('keyup', function() {
            if ($('#aboutus-user-name').val() != "" && $('#aboutus-user-data').val() != "" && $('#aboutus-user-email').val() != "") {
                $('#aboutus-submit-form').css({
                    'background-color': '#e0406e',
                    'pointer-events': 'all'
                })

            } else {
                $('#aboutus-submit-form').css({
                    'background-color': '#e1e1e6',
                    'pointer-events': 'none'
                })

            }

        })
        //----------------------about us form validation--------------------//
    $('#aboutus-submit-form').on('click', function(e) {
            e.preventDefault();
            var aboutUserName = $('#aboutus-user-name').val();
            var aboutUserData = $('#aboutus-user-data').val();
            var aboutUserEmail = $('#aboutus-user-email').val();
            if (dataValidate(aboutUserName, aboutUserData, aboutUserEmail)) {
                $('.aboutus-main-form').css('display', 'none')
                $('.aboutus-response-container').css('display', 'block')
            }
        })
        //---------------------- about usresponse container button function--------------------//
    $('#aboutus-response-button').click(function() {
        $('.aboutus-main-form').css('display', 'block')
        $('.aboutus-response-container').css('display', 'none')
        window.location.href = 'aboutus.html';
        document.querySelector('.aboutus-form').reset();

    })
    var emailRegex = /^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-zA-Z]{2,4}$/;
    //contact us form validation function----------------//
    function dataValidate(aboutUserName, aboutUserData, aboutUserEmail) {
        if (aboutUserName.length == "") {
            return false;
        }
        if ((emailRegex.test(aboutUserEmail))) {
            return true;

        } else {
            $('.aboutus-email-feild').append('<p class="form-error">' + 'Sorry, this doesn’t look like a valid email' + '</p>')
            $('#aboutus-user-email').toggleClass('invalid-email')
            return false;
        }

        return true
    }
    $('#aboutus-response-button').on('click', function() {
        window.location.href = 'aboutus.html'
    })

    //aboutus counter

    // $('.count').counterUp({

    //     delay: 10,
    //     time: 2000

    // });

});